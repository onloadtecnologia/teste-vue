import {createRouter, createWebHashHistory, createWebHistory} from 'vue-router';
import Cadastro from '../pages/Cadastro.component';
import Home from '../pages/Home.component';
import Sobre from '../pages/Sobre.component';


const routes = [
    { path: '/', component: Home },
    { path: '/cadastro', component: Cadastro },
    { path: '/sobre', component: Sobre },
  ]
  
  // 3. Create the router instance and pass the `routes` option
  // You can pass in additional options here, but let's
  // keep it simple for now.
  const router = createRouter({
    // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
    history: createWebHistory(),
    routes, // short for `routes: routes`
  })
  


  export default router;








