
import { ref } from 'vue'

const Cadastro = {
    name:"cadastro",
    setup(){
        const myfiles:Set<File> = new Set<File>();
        const formData:FormData = new FormData();         
        return{
            myfiles:ref(myfiles),
            formData:ref(formData)
        }
    },
    data:()=>({
        dados:{
            name:null,
            email:null,
            anexo:File
        }

    }),
    template:`
        <section class="container">
            <form v-on:submit.prevent="salvar()" class="form col-sm-10 col-lg-6 mx-auto">
                <div class="mb-3">
                    <label for="name" class="form-label">Nome</label>
                    <input type="text" v-model="dados.name" name="name" id="name" class="form-control">
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" v-model="dados.email" name="email" id="email" class="form-control">
                </div>
                <div class="mb-3">
                    <label for="anexo" class="form-label">Anexo</label>
                    <input type="file" v-on:change="getfile($event)" name="anexo" id="anexo" class="form-control">
                </div>
                <div class="mb-3 d-flex flex-column justify-content-between align-items-center">
                    <div class="col-12 text-center" v-for="(arquivos,index) in myfiles" :key="index">{{arquivos.name}}</div>                
                </div>
                <div class="mb-3">
                <button type="submit" class="btn btn-primary d-flex">
                    <i class="material-icons me-2">send</i>
                    <span>SALVAR</span>
                </button>        
                </div>
            </form>
        </section>
    `,
    methods:{
        getfile(event:any){
            this.myfiles.add(event.target.files[0])
        },
        salvar(){

          this.formData.append("name",this.dados.name);  
          this.formData.append("email",this.dados.email);

          this.myfiles.forEach(file => {
                this.formData.append("anexos[]",file,file.name)                  
          });

          this.formData.forEach(x=>{
            console.log(x)
          })         

        }
    }
}



export  default Cadastro; 