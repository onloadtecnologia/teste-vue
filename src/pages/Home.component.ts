import { ref } from "vue";
const el = ref<HTMLDivElement | null>(null);

const Home = {
      name: "home",

      setup() {


            return {
                  el: ref(el)

            }


      },
      data: () => ({

      }),
      watch: {
            el(newValue: HTMLDivElement, oldValue: HTMLDivElement) {


            }
      },
      mounted() {

            el.value!.style.color = "red"

      },
      methods: {

            teste: (event: any) => {
            }

      },
      template: `
      <section class="container">   
            <div class="text-center" ref="el">Home</div>
      </section>
  
      `
}



export default Home; 